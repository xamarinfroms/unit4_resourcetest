﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Unit4_ResourceTest
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MyResource : ContentPage
    {
        public MyResource()
        {
            InitializeComponent();

           // this.txtPwd.TextColor = (Color)this.Resources["EntryColor"];

           // this.txtPwd.SetDynamicResource(Entry.TextColorProperty, "EntryColor");
          
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            this.Resources["MyColor"] = Color.Red;
        }
    }
}
